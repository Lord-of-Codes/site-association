---
title: "Les Bioplastiques"
date: 2019-03-06T20:19:22+02:00
publishdate: 2019-03-06T20:19:22+02:00
image: "/img/blog/bioplastiques/bioplastiques.jpg"
tags: ["Environnement"]
comments: false
---
`Écrit par Alexia Mercier pour Bicursiosité` 

*Le plastique c'est fantastique, Le caoutchouc super doux, Nous l'affirmons sans complexe, Nous sommes adeptes du latex.” - Elmer Food Beat*

On en parle énormément ces derniers temps, en partie parce que la Chine est passée à l’offensive avec son opération *National Sword* en janvier 2018. Soudain, les pays d’Europe et d’Amérique du Nord se trouvent [privés de la voie principale de traitement de leurs plastiques](https://www.theguardian.com/environment/2018/jan/02/rubbish-already-building-up-at-uk-recycling-plants-due-to-china-import-ban), la [réalité de leur recyclage](https://www.youtube.com/watch?v=v0Kif9cugQ0 ) éclate, et le grand public est mis face à certaines conséquences d’un modèle de consommation encourageant le (sur)emballage et l’usage unique. Face à cette mauvaise presse, de nouveaux types de plastique ont la côte : les bio-plastiques. Mais que sont-ils, et apportent-ils une solution à la pollution plastique liée aux emballages ? 

# Des problèmes de définition

## Le terme bioplastique 
Bioplastique … Ca sonne bien non ? Mais c’est quoi au juste, un bioplastique ? 
Derrière cette question anodine se cache déjà une confusion, car il n’existe pas encore de définition normée. L’industrie et la recherche admettent généralement que le préfixe bio- du terme indique qu’il est biosourcé, ce qui est différent de biodégradable. Un plastique biosourcé est un plastique issu de biomasse renouvelable, mais il n’est pas forcément biodégradable, c’est à dire décomposable en CO2, CH4, H2O et biomasse par des micro-organismes, qui peuvent assimiler ces produits en tant que nutriments. Par exemple, le polyéthylène à base de canne à sucre est biosourcé mais non biodégradable, et le PBAT, utilisé dans les emballages agroalimentaires, est un plastique non-biosourcé mais biodégradable. Or cette différence n’est pas toujours claire pour le consommateur, ce qui peut créer une confusion potentiellement exploitable dans le cadre [green-washing](https://fr.m.wikipedia.org/wiki/%C3%89coblanchiment)

{{< figure src="/img/blog/bioplastiques/biodegradable biosource.jpg" title="">}}

*Graphique Biodégradable/Biosourcé par European Bioplastics*  
Le graphique ci-dessus montre que les attributs des bioplastiques ne sont pas nécessairement superposables.

{{< figure src="/img/blog/bioplastiques/applications bioplastique.jpg" title="">}}

Comme on peut le voir sur le graphique ci-dessus, les emballages représentaient près de 59% des applications des bioplastiques biodégradables en 2018. Même si les bioplastiques représentaient [moins d’1% du marché des plastiques](http://natureplast.eu/le-marche-des-bioplastiques/production-des-bioplastiques/) en 2017, il est fort probable que vous ayez déjà eu entre vos mains ces plastiques à base de maïs ou de canne à sucre dans le cas de pots de produits cosmétiques ou de bouteilles, de films et de barquettes pour fruits et légumes, ou encore de couverts et gobelets jetables.

## Les normes, labels et logos
*“Que signifie un 4 dans un triangle ? Et du vert, c’est plutôt bon signe non ?”*
Vous êtes-vous déjà retrouvé, emballage à la main, déterminé mais démuni devant la multitude de logos imprimés dessus ? Si ce n’est pas le cas et que vous n’avez jamais flanché, il est temps d’ouvrir votre hotline d’urgence pour vos confrères dans le besoin… Il existe en effet beaucoup de logos et labels parfois redondants ou manquant de clarté. La confusion qu’ils entraînent peut porter préjudice à l’utilisation et à la fin de vie des bioplastiques. Saviez-vous par exemple qu’il existe une différence entre la compostabilité d’un emballage dans votre jardin et sa compostabilité dans une installation industrielle ? Ces distinctions se font par l’intermédiaire de normes comme la [NF EN 13432 qui défini la biodégradabilité et la compostabilité d’un emballage](https://desbonneschoses.weebly.com/uploads/2/1/8/3/21832610/brve_description_de_la_norme_en_13432.pdf). Ces normes et standards sont essentiels mais ont parfois du mal à suivre le pas de l’innovation, ce qui, comme nous le verrons plus tard, peut devenir problématique.

{{< figure src="/img/blog/bioplastiques/compost_et_symboles.jpg" title="">}}

*Gauche: Infographie par Gaël Nicolet pour L’Info Durable; Droite: Labels OK Compost et OK Compost Home par Vinçotte*


# La fin de vie des emballages en bioplastiques
Qu’arrive t-il aux bioplastiques au moment d’être jetés et traités ? Les bioplastiques sont-ils à placer avec les autres matériaux recyclables ? Que faire de la distinction biodégradable/non-biodégradable ? Nous avons vu précédemment que les confusions liées aux définitions et logos pouvaient porter préjudice à la fin de vie des bioplastiques. Nous allons maintenant voir comment les incompatibilités chimiques et les motivations économiques rendent difficile l’instauration d’une filière des bioplastiques dans les flux de déchets.

### Les bioplastiques non-biodégradables
Considérons le cas des bioplastiques non-biodégradables, ceux que l’on pourrait à priori placer dans la poubelle des recyclables. 
Il faut savoir qu’il n’est malheureusement pas possible de mélanger et fondre ensemble tous les types de plastiques puis de les mouler en de nouveaux objets. Une compatibilité des structures chimiques, (sans parler des additifs et des couleurs) est nécessaire, et même pour les plastiques “traditionnels”, ces compatibilités ne sont pas monnaie courante. Il est donc obligatoire de trier et séparer chaque type de plastique pour assurer une continuité dans les performances et propriétés des plastiques recyclés. La contamination d’une fournée de plastique X par un plastique Y aura de graves conséquences, car le nouveau matériau formé aura des propriétés indésirables ou tout simplement imprévisibles, limitant toute réutilisation: une situation inacceptable pour l’industrie.  
Rappelons maintenant que les bioplastiques représentent seulement une faible proportion des déchets plastiques. Combinés aux limites de compatibilité chimique et au risque de contamination, il n’est donc pas rentable de leur dédier des flux spécifiques de recyclage. Ainsi lorsqu’ils arrivent en centre de tri et de recyclage, ils sont rejetés par défaut et envoyés en décharge.

### Les bioplastiques biodégradables 

Même dans le cas des bioplastiques biodégradables, la grande majorité des installations industrielles de compost refusent ce type de déchet car il n’apporte pas de valeur ajoutée d’un point de vue agronomique. De plus, la décomposition des bioplastiques est plus longue que celle des biodéchets. Cela signifie qu’il doit y avoir, soit un ralentissement de la production, soit que le compost vendu présentera encore des résidus de plastiques, au risque de rebuter le consommateur. Enfin, accepter les bioplastiques c'est aussi prendre le risque de contaminer le compost avec des plastiques non biodégradables et peu différentiables, et donc de complexifier le fonctionnement des installations ou d'affecter la qualité du compost.    
Il manque des données opérationnelles (à opposer aux données de laboratoire qui ne reflètent pas les conditions réelles) qui pourraient relativiser les contraintes et encourager l’industrie à développer des voies de valorisation pérennes.
Néanmoins les efforts s’organisent ! 

L’été dernier, Yumi, un producteur de jus de fruit vendus dans des bouteilles compostables, s’est vu sous la menace de payer un malus écologique car ses bouteilles étaient triées comme des indésirables et [finissaient donc en incinérateur](https://www.nouvelobs.com/planete/20180615.OBS8200/c-est-l-histoire-d-une-bouteille-biodegradable-que-l-etat-ne-veut-pas-biodegrader.html). Interpellés par ce cas d’école, [les Alchimistes](https://alchimistes.co/), producteurs de compost en circuit court et en logistique douce dans Paris, ont lancé une expérimentation pour étudier la décomposition des bouteilles de jus de fruit en bioplastique PLA (acide polylactique) dans leur compost. L’expérimentation, dont les résultats sont concluants, a néanmoins soulevé des questions importantes: est-ce que les protocoles d’analyse du compost sont à jour pour prendre en compte l’arrivée de “nouveaux” matériaux comme le PLA ? Est-ce que le PLA et les potentiels fragments résiduels sont à considérer comme inertes-plastiques et sont donc sujets aux [normes et limitations](https://wiki.aurea.eu/index.php/NF_U_44-051) sur le compost, ou est ce que leur nature biodégradable les exclue de la catégorie inerte ? De plus, ces normes ont été formulées il y a plus d’une dizaine d’année, lorsque le terme de plastique était sans équivoque synonyme de non-biodégradabilité… On en revient donc aux problèmes de définition. 


# Mais alors, une vraie fausse bonne idée ?

*“Noir c’est noir, il n’y a plus d’espoir” - Johnny*  
Avant de partir défaitiste et de mettre les bioplastiques “à la poubelle”, il faut étudier les nuances et les enjeux de l’emballage.  

## Analyse du cycle de vie 
Comparer les propriétés des différents matériaux nécessite de regarder l’ensemble de la chaîne de fabrication, d’utilisation et enfin de recyclage/valorisation, et de calculer l’impact total de notre matériaux le long de son existence ! C’est tout l’enjeu des analyses de cycle de vie (ACV et en anglais LCA). Souvent, les conclusions tirées de ces analyses sont moins évidentes qu’elles n’y paraissent ! Ici, nous pouvons commencer une analyse conceptuelle grossière : 


*  Dans le cas des **plastiques biosourcés mais pas biodégradables**, la production de biomasse pour certains bioplastiques peut entrer en compétition avec l’allocation des terres et les productions alimentaires puisque que certains utilisent des matières premières alimentaires (ex : maïs, canne à sucre), ce qui n’est pas sans rappeler la [controverse du biodiesel au Brésil](http://news.bbc.co.uk/2/hi/business/7026105.stm) d’il y a quelques années. On obtient au final un produit difficilement valorisable à la fin de sa vie et qui a de fortes chances de s’accumuler dans l’environnement. Il faut aussi prendre en compte l’impact de l’agriculture sur l'érosion des sols, la pollution des sols et des eaux par l’ajout d’engrais et de pesticides, la main d’oeuvre qui ne travaille pas systématiquement dans des conditions humaines, le transport de la matière première et du produit fini, la consommation d’énergie …    
*  Dans le cas des **plastiques biosourcés et biodégradables**, il semble y avoir des aspects compensatoires. Il y a une voie de valorisation qui permet d’empêcher l’accumulation des plastiques, qui sont [difficilement récupérables](https://www.wired.com/story/ocean-cleanups-plastic-catcher/) une fois dans l’environnement. Les produits de dégradation sont théoriquement inoffensifs pour les écosystèmes et la restitution de la matière organique aux sols produit un bilan carbone virtuellement neutre. 

En réalité, ces calculs sont longs et fastidieux et l’analyse que nous venons de faire est extrêmement limitée. Voici en image ci-dessous un exemple qui montre l’intérêt majeur de l’ACV. L’illustration indique le nombre de fois qu’un sac de course devrait être réutilisé en fonction de son matériau de base pour rentabiliser son impact environnemental comparé au sac plastique à usage unique, dont la distribution gratuite est aujourd’hui interdite. 

{{< figure src="/img/blog/bioplastiques/grocerybaglca.png" title="">}}

## L’éco-conception 
L’écoconception vise à [« intégrer des aspects environnementaux dans la conception et le développement de produits »](https://www.ademe.fr/entreprises-monde-agricole/organiser-demarche-environnementale/dossier/ecoconcevoir-produits/enjeux-lecoconception-benefices-lentreprise-leconomie-lenvironnement). C’est une approche essentielle pour minimiser l’impact environnemental d’un produit en optimisant les choix de matériaux et en prenant en compte tous les aspects de la vie du produit.
Quel est par exemple le meilleur choix d’emballage pour un yaourt ? Pour concevoir son emballage, il faut prendre en compte la disponibilité des matériaux existants, si besoin investir en Recherche & Développement, intégrer les contraintes liées aux procédés industriels (coûts pour production et pour retraitement) et prendre en compte les habitudes du consommateur. Mettra-t-il spontanément son pot de yaourt dans le bac à épluchures s’il est compostable ou dans le bac à verre s’il est en verre ? Que fera t-il de l’opercule qui est souvent un mélange complexe de film en plastique et en aluminium ? C’est ainsi qu’on comprend qu’il n’y a pas de solution miracle, mais un choix pragmatique à faire, spécifique à chaque type de produit.


# Conclusion
La question de la pollution liée aux emballages plastiques ne connaît pas de solution simple. Le meilleur déchet étant celui qu’on ne produit pas, des efforts sont à faire sur les modèles de vente et de distribution entre les entreprises et entre les entreprises et les consommateurs pour réduire au maximum la nécessité des emballages plastiques. Pour ceux dont on ne pourrait se passer, l’intégration des principes de l’éco-conception et de l’ACV pourra minimiser les impacts environnementaux et révéler les meilleurs cas d’utilisation des bioplastiques.
Il y a enfin besoin d'un travail important de communication et de coopération entre tous les acteurs de la chaîne (institutions légales, centres de recherche, industrie de production collecte et traitement, consommateurs) pour mener une action coordonnée et efficace contre la pollution liée aux emballages plastiques.

### Pour aller plus loin : 
* https://ourworldindata.org/plastic-pollution 
* https://www.ted.com/talks/leyla_acaroglu_paper_beats_plastic_how_to_rethink_environmental_folklore?language=en 
* https://www.european-bioplastics.org/ 
