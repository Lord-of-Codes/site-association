---
title: "Hannah - Regards d'une bicu du monde"
date: 2019-09-17T18:38:12+02:00
publishdate: 2019-09-17T18:38:12+02:00
image: "/img/blog/bicudumonde.jpg"
tags: ["International"]
comments: false
---
`Entretien par Alexia Mercier pour Bicursiosité` 

*J'ai eu la chance de travailler plusieurs mois à l'Université de Las Vegas (UNLV) où j'ai fait la rencontre de Hannah Patenaude, une étudiante américaine qui poursuit elle aussi un bicursus à sa façon. Curieuse de pouvoir comparer son expérience avec mes souvenirs du double cursus Chimie-Histoire, je lui ai demandé de me décrire son parcours et ses projets. Bonne lecture !*


##### Hello Hannah, so first things first, what are the degrees you're pursuing?

As a fifth-year senior undergraduate, I am making the most of my time in college by pursuing two degrees: **Chemistry**, Bachelor of Science and **Communication Studies**, Bachelor of Arts at the Honors College[^1] of the [University of Nevada, Las Vegas](https://www.unlv.edu/). After graduation in May, I intend on pursuing a graduate program for a doctoral degree in radiochemistry (hopefully at UNLV!). 

##### As first-year students of the double major programs at Sorbonne University, we'd often ask each others how we ended up there and why. Talking with each other we'd realise we all had different reasons! So let me ask you: How did you end up doing your two degrees?

When I started college, I was a biochemistry major and was preparing for medical school, but I was starting to second-guess my future and whether it was what I was truly passionate about. I decided to take a course for my honors requirements called the Rhetoric of Science and Medicine that was relevant for other reasons at the time. In the final week of that class, we visited the [National Atomic Testing Museum](https://nationalatomictestingmuseum.org/) where we learned about the Manhattan Project from the lense of public communication. I walked out of the museum and immediately emailed my advisor to change my major to chemistry. After learning about the Radiochemistry Program at UNLV where you can earn a Doctor of Philosophy (Ph.D.) in Radiochemistry, I joined [their team](https://www.unlv.edu/chemistry/radiochemistry) as an undergraduate researcher where I have been for nearly three years. This is also the program I am hoping to join after graduation.
My interest in rhetoric only grew as I decided to first minor in communications, but then eventually sign up for a full second degree. It has become increasingly clear how important the transmission of information is for any field of science, but especially ones surrounded in controversy like nuclear energy. I have also taken part in research projects through the Communication Studies Department at UNLV where I study the state of the rhetoric of nuclear energy in the United States. 

##### Back in the day, I remember history classmates eyeing our maths notes in distress, and some faculties being hostiles because we had to sacrifice some of their lectures in order to attend the other major's tutorials. May I ask how your endeavour is perceived by your classmates, faculty, and people outside of Uni?

My peers, mentors, and professors have all come to understand the value of communicating their science to the public for it to be most impactful. The rhetoricians in my life appreciate my incorporation of their research into less traditional applications and fully support my endeavors. After taking a course for my communication studies degree about the rhetoric of science (different from the first class), I have come to learn that it is nearly impossible to incorporate your research without communicating it in some capacity. Whether it’s writing a grant to fund your project, the paper you synthesize to share your results or just talking to the general public about why it matters, you are communicating and you must do it thoughtfully.


##### Our students are often asked what they're going to do with their two degrees, and as you can imagine, there's rarely a straightforward answer. So I'm curious. How do you intend to valorize this curriculum in your future career?

It is my hope that the image of studying STEM (science, technology, engineering, and mathematics) will continue to hybridize with social sciences like communications, psychology, and philosophy to create well-rounded scientists. I intend to continue my research about the rhetoric of nuclear energy as well as other nuclear science applications into my technical program for radiochemistry as well as teaching undergraduate chemists with an emphasis on communication during my graduate studies. While I hope to continue research in radiochemistry after my studies are completed, I would like to find my way into nuclear policy at the federal and international scales. Near the end of my career, I may circle back to academia to teach the next generation of nuclear scientists.

##### Since you're doing communication studies, what are the caveats of science communication according to you?

One of the biggest caveats in studying science communication is understanding the concept of spheres. We have three main spheres of communication: public, private, and technical. The public sphere contains the general public and is used for mass media and transmission of ideas to the general populous. The private sphere has one-on-one conversations with personal aspects. The technical sphere is where science communication becomes interesting as it is composed of field-specific jargon used by technical experts. The problems occur when information moves between those spheres, say from a scientific paper to a newspaper article, there may be problems in the translation process and the ability for the readers to digest complex concepts without proper interpretation. Additionally, there is not an active push to integrate cutting-edge papers published by scientists into the public sphere after converting jargon into layperson terms. Because of this, it is relatively easy for big industries like fossil fuel companies to take advantage of the power of rhetoric. They can create campaigns that are strategic in pro-fossil fuels or create distractions about resources like nuclear.


##### Definitely food for thoughts... Hannah, thank you so much for your time, and for sharing your experience with us. 

*Si vous souhaitez rentrer en contact avec Hannah, n'hésitez pas à nous faire signe, et nous vous mettrons en contact avec plaisir !*

###### Notes 

[^1]: Les Honors College sont une particularité du système universitaire américain. Imaginez un département qui sélectionne des étudiants en fonction de leur excellence académique, et qui leur offre/impose certains cours supplémentaires que les autres cursus n'ont pas. L'appartenance à un Honors college est souvent associée à l'accès à des bourses d'études spécifiques. 
